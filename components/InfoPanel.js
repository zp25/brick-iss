var React = require('react');

module.exports = InfoPanel = React.createClass({
  render: function() {
    var astronItem = function(item, index) {
      return <li key={index + item.name} craft={item.craft}>{item.name}</li>;
    };
    var passtimeItem = function(item, index) {
      var date = new Date(item.risetime*1000);
      return <li key={index + item.risetime}>{date.toString()}</li>;
    };

    return (
      <div className="infoPanel">
        <div className="info">
          <span><b>Current ISS Location: </b></span>
          <span>{ this.props.isslatlng.lat + '° N, ' + this.props.isslatlng.lng + '° E' }</span>
        </div>

        <div className="info">
          <span><b>Marker Location: </b></span>
          <span>{ this.props.latlng.lat + '° N, ' + this.props.latlng.lng + '° E'}</span>
        </div>

        <div className="info">
          <span><b>There are currently { this.props.astronauts.length } humans in space: </b></span>
          <ul>{ this.props.astronauts.map(astronItem) }</ul>
        </div>

        <div className="info">
          <span><b>Five upcoming ISS passes for Portland Oregon: </b></span>
          <ul>{ this.props.passtime.map(passtimeItem) }</ul>
        </div>
      </div>
    );
  }
});