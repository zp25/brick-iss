var React = require('react');
var L = require('leaflet');

module.exports = MapView = React.createClass({

  onLocationFound: function(e) {
    var radius = e.accuracy / 2;
    var marker = L.marker(e.latlng).addTo(this.map);

    marker.bindPopup("You are within " + radius + " meters from this point")
          .openPopup();

    this.props.onMarkerUpdate(e.latlng);
  },

  onLocationError: function(e) {
    var latlng = { lat: 51.5, lng: -0.09 };
    var marker = L.marker(latlng, { draggable:true }).addTo(this.map);

    this.props.onMarkerUpdate(latlng, e.message);

    marker.on("dragend", this.handleDragend);
  },

  handleDragend: function(e) {
    var marker = e.target;
    var latlng = marker.getLatLng();

    marker.setLatLng(latlng);

    this.props.onMarkerUpdate(latlng);
  },

  componentDidMount: function() {
    var map = L.map(React.findDOMNode(this)).setView(this.props.latlng, 2);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      attribution: '<a href="https://www.mapbox.com/about/maps/" target="_blank">&copy; Mapbox &copy; OpenStreetMap</a>',
      maxZoom: 4,
      id: 'zp.88395762',
      accessToken: 'pk.eyJ1IjoienAiLCJhIjoiLV93YTVsTSJ9.tQ5Si2p6ztRjrSZNGAQc-g'
    }).addTo(map);

    this.map = map;

    // locate
    map.locate();

    map.on('locationfound', this.onLocationFound);
    map.on('locationerror', this.onLocationError);
  },

  componentDidUpdate: function(prevProps, prevState) {
    var iss = prevProps.isslatlng;
    var ISSIcon;

    if (iss.lat!==this.props.isslatlng.lat || iss.lng!==this.props.isslatlng.lng) {

      if (!this.iss && !this.isscirc) {
        // ISS markers
        ISSIcon = L.icon({
          iconUrl: 'icon/ISSIcon.png',
          iconSize: [50, 30],
          iconAnchor: [25, 15],
          popupAnchor: [50, 25],
          shadowUrl: 'icon/ISSIcon_shadow.png',
          shadowSize: [60, 40],
          shadowAnchor: [30, 15]
        });

        this.iss = L.marker(this.props.isslatlng, { icon: ISSIcon }).addTo(this.map);
        this.isscirc = L.circle(this.props.isslatlng, 2200e3, { color: "#c22", opacity: 0.3, weight:1, fillColor: "#c22", fillOpacity: 0.1 }).addTo(this.map);
      }

      this.iss.setLatLng(this.props.isslatlng);
      this.isscirc.setLatLng(this.props.isslatlng);

      this.map.panTo(this.props.isslatlng, animate=true);
    }
  },

  render: function() {
    return (
      <div className="mapView"></div>
    );
  }

});