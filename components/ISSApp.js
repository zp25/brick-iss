var React = require('react');
var _ = require('underscore');
var Firebase = require('firebase');
var MapView = require('./MapView');
var InfoPanel = require('./InfoPanel');

module.exports = ISSApp = React.createClass({

  getInitialState: function() {
    return {
      latlng: this.props.marker,
      isslatlng: this.props.iss,
      astronauts: [],
      passtime: []
    };
  },

  markerUpdate: function(latlng, msg) {
    var latlngFixed = _.mapObject(latlng, function(val, key) { return val.toFixed(3); });

    this.setState({
      latlng: latlngFixed
    });

    if (msg) {
      console.log(msg + ' Use default location.');
    }

    this.issPassTime();
  },

  moveISS: function() {
    $.ajax({
      method: 'GET',
      url: 'http://api.open-notify.org/iss-now.json?callback=?',
      contentType: false,
      dataType: 'jsonp',
      context: this
    })
    .done(function(data) {
      var lat = data['iss_position']['latitude'].toFixed(3);
      var lng = data['iss_position']['longitude'].toFixed(3);

      this.setState({
        isslatlng: { lat: lat, lng: lng }
      });
    })
    .fail(function() {
      console.log('Get ISS Location Failed');
    });
  },

  issPassTime: function() {
    var qs = 'lat=' + this.state.latlng.lat + '&lon=' + this.state.latlng.lng + '&alt=20&n=5';
    var url = 'http://api.open-notify.org/iss-pass.json?' + qs + '&callback=?';

    $.ajax({
      method: 'GET',
      url: url,
      contentType: false,
      dataType: 'jsonp',
      context: this
    })
    .done(function(data) {
      this.setState({
        passtime: data['response']
      });
    })
    .fail(function() {
      console.log('Get ISS Pass Times Failed');
    });
  },

  peopleInSpace: function() {
    $.ajax({
      method: 'GET',
      url: 'http://api.open-notify.org/astros.json?callback=?',
      contentType: false,
      dataType: 'jsonp',
      context: this
    })
    .done(function(data) {
      this.setState({
        astronauts: data['people']
      });
    })
    .fail(function() {
      console.log('Get Number of People In Space Failed');
    });
  },

  savePosition: function() {
    var pos = {};
    var ref = new Firebase("https://fiery-inferno-7279.firebaseio.com/Position");

    pos = _.extend(pos, this.state.latlng);
    pos.response = this.state.passtime;

    ref.push(pos, function(err) {
      if (err)
        console.log("Position could not be saved. " + err);
      else
        console.log("Position saved successfully.");
    });
  },

  componentDidMount: function() {
    this.interval = setInterval(this.moveISS, 5000);

    this.peopleInSpace();
  },

  componentWillUnmount: function() {
    clearInterval(this.interval);
  },

  render: function() {
    return (
      <div id="brick-iss">
        <MapView latlng={this.state.latlng} isslatlng={this.state.isslatlng} onMarkerUpdate={this.markerUpdate}></MapView>
        <InfoPanel {...this.state} ref="infoPanel"></InfoPanel>
        <button type="button" onClick={this.savePosition}>Save</button>
      </div>
    );
  }

});