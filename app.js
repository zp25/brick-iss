var React = require('react');
var _ = require('underscore');
var Firebase = require('firebase');
var L = require('leaflet');
var ISSApp = require('./components/ISSApp');

// specify the path to the leaflet images folder
L.Icon.Default.imagePath = 'leaflet/images/';

// Snag the initial state that was passed from the server side
var initialState = JSON.parse(document.querySelector('#initial-state').innerHTML)

// Render the components, picking up where react left off on the server
React.render(
  <ISSApp {...initialState}/>,
  document.querySelector('#react-app')
);