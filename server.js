var express = require('express');
var http = require('http');
var exphbs = require('express-handlebars');

var routes = require('./routes');

// Create an express instance and set a port variable
var app = express();
var port = process.env.PORT || 8080;

// Set handlebars as the templating engine
app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Routes
app.get('/', routes.index);

app.use("/", express.static(__dirname + "/public/"));
app.use("/leaflet", express.static(__dirname + "/node_modules/leaflet/dist/"));

var server = http.createServer(app).listen(port, function() {
  console.log('Express server listening on port ' + port);
});