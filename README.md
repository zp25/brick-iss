Brick ISS

## Open Notify API
International Space Station Current Location

[http://api.open-notify.org/iss-now.json][issnow]

    {
      "message": "success", 
      "timestamp": UNIX_TIME_STAMP, 
      "iss_position": {
        "latitude": CURRENT_LATITUDE, 
        "longitude": CURRENT_LONGITUDE
      }
    }

How Many People Are In Space Right Now

[http://api.open-notify.org/astros.json][astros]

    {
      "message": "success",
      "number": NUMBER_OF_PEOPLE_IN_SPACE,
      "people": [
        {"name": NAME, "craft": SPACECRAFT_NAME},
        ...
      ]
    }

International Space Station Pass Times

[http://api.open-notify.org/iss-pass.json?lat=LAT&lon=LON][isspass]

    {
      "message": "success",
      "request": {
        "latitude": LATITUE,
        "longitude": LONGITUDE, 
        "altitude": ALTITUDE,
        "passes": NUMBER_OF_PASSES,
        "datetime": REQUEST_TIMESTAMP
      },
      "response": [
        {"risetime": TIMESTAMP, "duration": DURATION},
        ...
      ]
    }

[http://open-notify.org/Open-Notify-API/][onapi]


# 参看
+ [Open Notify](http://open-notify.org/ "Open Notify")
+ [International Space Station - Spot The Station](http://spotthestation.nasa.gov "International Space Station - Spot The Station")


[issnow]:http://api.open-notify.org/iss-now.json "ISS Now"
[astros]:http://api.open-notify.org/astros.json "Astronaut"
[isspass]:http://api.open-notify.org/iss-pass.json?lat=LAT&lon=LON "ISS Pass"
[onapi]:http://open-notify.org/Open-Notify-API/ "Open Notify API"