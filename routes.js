// var JSX = require('node-jsx').install();
// var React = require('react');
// var ISSApp = require('./components/ISSApp');

module.exports = {

  index: function(req, res) {
    var initialState = {
      marker: { lat: 0, lng: 0 },
      iss: { lat: 0, lng: 0 }
    };

    // Server side render
    // var markup = React.renderToString(
    //   ISSApp({
    //     initialState: initialState
    //   })
    // );

    // Render 'home' template
    res.render('home', {
      state: JSON.stringify(initialState)
    });
  }

}